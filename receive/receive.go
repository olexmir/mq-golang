package main

import (
	"log"
	"github.com/streadway/amqp"
	"../shared"
	"gopkg.in/mgo.v2"
	"encoding/json"
)

func main() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	shared.FailOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	session, err := mgo.Dial("mongodb://localhost:27017/myproject")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	ch, err := conn.Channel()
	shared.FailOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"beerToDB", // name
		false,   // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	shared.FailOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	shared.FailOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			log.Printf("Received a message: %s", d.Body)
			saveToDB(session, d.Body)
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}

func saveToDB(session *mgo.Session, body []byte)  {
	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)

	c := session.DB("test").C("beer")
	var beer shared.Beer
	json.Unmarshal(body, &beer)
	err := c.Insert(beer)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Data was inserted")
}