## Example of http request:
```
{
	"type": "dark",
	"name": "Obolon",
	"manufacturer": "Obolon Kyiv",
	"chars": {
		"turnovers": 10.2,
		"opacity": 20.2,
		"nutrition": 120,
		"filter": true
	}
}
```

## Docker
```
version: '3'
services:
  mongodb:
    image: mongo
    ports:
      - "27017:27017"
  queue:
    image: rabbitmq:latest
    environment:
      - RABBITMQ_DEFAULT_USER=
      - RABBITMQ_DEFAULT_PASS=
    ports:
      - "5672:5672"
```


# Delete all containers
```docker rm $(docker ps -a -q)```
# Delete all images
```docker rmi $(docker images -q)```