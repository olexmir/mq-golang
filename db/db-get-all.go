package main

import (
	"gopkg.in/mgo.v2"
	"log"
	"fmt"
	"mq/shared"
)

func main() {
	session, err := mgo.Dial("mongodb://localhost:27017/myproject")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB("test").C("beer")

	var results []shared.Beer
	err = c.Find(nil).All(&results)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("All results:", results)
}