package send

import (
	"log"
	"github.com/streadway/amqp"
	"../shared"
)

func Send(beer []byte) {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	shared.FailOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	shared.FailOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"beerToDB", // name
		false,   // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	shared.FailOnError(err, "Failed to declare a queue")

	body := beer
	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        beer,
		})
	log.Printf(" [x] Sent %s", body)
	shared.FailOnError(err, "Failed to publish a message")
}