package main

import (
	"net/http"
	"log"
	"encoding/json"
	"../shared"
	"../send"
)

type JSONError struct {
	Err string
	Code int
}

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe("localhost:8000", nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		output, err := json.Marshal(&JSONError{Err: "only post supported", Code: 401})
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		w.Header().Set("content-type", "application/json")
		w.Write(output)
	}

	decoder := json.NewDecoder(r.Body)
	var beer shared.Beer
	err := decoder.Decode(&beer)
	if err != nil {
		panic(err)
	}
	defer r.Body.Close()

	j, err := json.Marshal(beer)

	if err != nil {
		log.Println("oops")
	}

	log.Println(beer)
	send.Send(j)
}

