package shared

type Beer struct {
	Type         string `json:"type"`
	Name         string `json:"name"`
	Manufacturer string `json:"manufacturer"`
	Chars        struct {
		Turnovers float64 `json:"turnovers"`
		Opacity   float64 `json:"opacity"`
		Nutrition int     `json:"nutrition"`
		Filter    bool    `json:"filter"`
	} `json:"chars"`
}
